import React from 'react';
import Breakpoint from './Breakpoint';

const breakpoints = {
  desktop: '(min-width: 1025px)',
  tablet: '(min-width: 768px) and (max-width: 1024px)',
  mobile: '(max-width: 767px)',
};

const Breakpoint = (props) => {
  const breakpoint = breakpoints[props.name] || breakpoints.desktop;
  return (
    <MediaQuery {...props} query={breakpoint}>
      {props.children}
    </MediaQuery>
  );
}

export const Desktop = (props) => {
  return (
    <Breakpoint name="desktop">
      {props.children}
    </Breakpoint>
  );
}

export const Mobile = (props) => {
  return (
    <Breakpoint name="mobile">
      {props.children}
    </Breakpoint>
  );
}

export const Tablet = (props) => {
  return (
    <Breakpoint name="tablet">
      {props.children}
    </Breakpoint>
  );
}
